import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { DetallesnotiRoutingModule } from "./detallesnoti-routing.module";
import { DetallesnotiComponent } from "./detallesnoti.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        DetallesnotiRoutingModule
    ],
    declarations: [
        DetallesnotiComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class DetallesnotiModule { }
