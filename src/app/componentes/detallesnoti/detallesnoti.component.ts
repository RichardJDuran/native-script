import { Component, OnInit } from '@angular/core';
import { DetalleProducto } from './../../domain/detalle.producto';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import * as app from "tns-core-modules/application";
import { Router } from '@angular/router';

@Component({
    selector: 'ns-detallesnoti',
    templateUrl: './detallesnoti.component.html',
    providers: [ DetalleProducto    ]
})

export class DetallesnotiComponent implements OnInit {



    constructor( private detalles : DetalleProducto, private router:Router) { }

ngOnInit() {
    this.detalles.agregar("detalle 1");
    this.detalles.agregar("detalle 2");
    this.detalles.agregar("detalle 3");

}


    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}

