import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { InfodetallesComponent } from "./infodetalles.component";

const routes: Routes = [
    { path: "", component: InfodetallesComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class InfodetallesRoutingModule { }
