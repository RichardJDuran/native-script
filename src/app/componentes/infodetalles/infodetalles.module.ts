import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { InfodetallesRoutingModule } from "./infodetalles-routing.module";
import { InfodetallesComponent } from "./infodetalles.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        InfodetallesRoutingModule
    ],
    declarations: [
        InfodetallesComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class InfodetallesModule { }
