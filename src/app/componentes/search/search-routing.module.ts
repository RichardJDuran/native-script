import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { SearchComponent } from "./search.component";
import { DetallesnotiComponent } from "./../detallesnoti/detallesnoti.component"

const routes: Routes = [
    { path: "", component: SearchComponent },
    {path: "detalle, component: DetallesnotiComponent"}
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class SearchRoutingModule { }
