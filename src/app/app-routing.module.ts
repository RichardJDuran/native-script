import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

const routes: Routes = [
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "home", loadChildren: () => import("~/app/componentes/home/home.module").then((m) => m.HomeModule) },
    { path: "browse", loadChildren: () => import("~/app/componentes/browse/browse.module").then((m) => m.BrowseModule) },
    { path: "search", loadChildren: () => import("~/app/componentes/search/search.module").then((m) => m.SearchModule) },
    { path: "featured", loadChildren: () => import("~/app/componentes/featured/featured.module").then((m) => m.FeaturedModule) },
    { path: "settings", loadChildren: () => import("~/app/componentes/settings/settings.module").then((m) => m.SettingsModule) },
    { path: "infodetalles", loadChildren: () => import("~/app/componentes/infodetalles/infodetalles.module").then((m) => m.InfodetallesModule) },
    { path: "detallesnoti", loadChildren: () => import("~/app/componentes/detallesnoti/detallesnoti.module").then((m) => m.DetallesnotiModule) }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
