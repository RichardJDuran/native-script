import { Component, OnInit } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { RouterExtensions } from "nativescript-angular/router";
import * as app from "tns-core-modules/application";
import { NoticiasService } from "../../domain/noticias.service";
import { ListViewEventData } from "nativescript-ui-listview";

@Component({
    moduleId: module.id,
    selector: "List",
    templateUrl: "./list.component.html"
})
export class ListComponent implements OnInit {

    constructor(
        private router: Router, 
        private routerExtensions: RouterExtensions, 
        private noticias: NoticiasService) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.noticias.agregar("Mercurio");
        this.noticias.agregar("Venus");        
        this.noticias.agregar("Tierra");
        this.noticias.agregar("Marte");
        this.noticias.agregar("Jupiter");
    }

    refreshList(args) {
        const pullRefresh = args.object;
        setTimeout(function () {
            this.noticias.agregar("Saturno");
            this.noticias.agregar("Urano");
            this.noticias.agregar("Neptuno");
            pullRefresh.refreshing = false;
        }, 1000);
    }
    onPullToRefreshInitiated(args: ListViewEventData) {
        setTimeout(function () {            
            console.log("comenzamos");
            this.noticias.agregar("Saturno");
            const listView = args.object;
            listView.notifyPullToRefreshFinished();
        }, 1000);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
    
    onItemTap(x){
        let n = this.noticias.buscar();
        let navItemRoute = '/news/' + x.index;
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
    }
}
